<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Test App</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <style>body {margin-top: 40px; background-color: #333;}</style>
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    </head>

    <body>
        <div class="container">
            <div class="hero-unit">
                <h1>Test App</h1>
		<h2>Your deploy worked!</h2>
                <p>Your PHP application is now running on the host &ldquo;<?php echo gethostname(); ?>&rdquo;.</p>
                <p>This host is running PHP version <?php echo phpversion(); ?>.</p>
		<p>
 	
<?php
include 'db_connect.php';
 
$mysql = mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
 
/* Test the MySQL connection */
if (mysqli_connect_errno()) {
printf("Connection failed: %s\n", mysqli_connect_error());
exit();
}
 
/* Print the MySQL server version */
printf("MySQL server version: %s\n<br \>\n", mysqli_get_server_info($mysql));

$query = "SELECT count(*) as total from " . $DB_TABLE;

$result=mysqli_query($mysql, $query);
while ($data = mysqli_fetch_assoc($result)) {
	//$data=mysqli_fetch_assoc($result);
	//echo $data['total'];
	printf("There are %s rows in the database.\n", $data['total']);
}

/* free result set */
mysqli_free_result($result);

/* Close the MySQL connection */
mysqli_close($mysql);
?>
		</p>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>

</html>
